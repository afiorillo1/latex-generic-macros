# latex-generic-macros
A collection of generically written LaTeX macros which can be imported into a document by using `\input{...}`
